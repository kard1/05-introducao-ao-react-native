import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  bigBlue: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },
  red: {
    color: 'red',
  },
  big: {
    fontSize:50,
  },
  big2: {
    fontSize:100,
  },
  big3: {
    fontSize:120,
  },
  });
class Blink extends Component {
  constructor(props) {
    super(props);
    this.state = { isShowingText: true };

    // Toggle the state every second
    setInterval(() => (
      this.setState(previousState => (
        { isShowingText: !previousState.isShowingText }
      ))
    ), 1000);
  }

  render() {
    if (!this.state.isShowingText) {
      return <Text>  </Text>;
    }

    return (
      <Text>{this.props.text}</Text>
    );
  }
}

export default class BlinkApp extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Blink text='Hello World!' />
        <Text>Hello World!</Text>
        <Text style={styles.red}>Hello World!</Text>
        <Text style={styles.bigBlue}>Hello World!</Text>
        <View style={{width: 150, height: 20, backgroundColor: 'steelblue'}}> 
        <Text >Hello World!</Text>
        </View>

        <View style={{flex: 1, backgroundColor: 'powderblue'}}>
          <Text style= {styles.big}>Hello World!</Text>
        </View>
        
        <View style={{flex: 2, backgroundColor: 'skyblue'}}>
        <Text style= {styles.big2}>Hello World!</Text>
        </View>
        <View style={{flex: 3, backgroundColor: 'steelblue'}}>
        <Text style= {styles.big3}>Hello World!</Text>
        </View>
        
         <View style = {width = 50}>
          <Blink text = 'H'/>
          <Blink text = 'e'/>
          <Blink text = 'l'/>
          <Blink text = 'l'/>
          <Blink text = 'o'/>
          <Blink text = ''/>
          <Blink text = 'w'/>
          <Blink text = 'o'/>
          <Blink text = 'r'/>
          <Blink text = 'l'/>
          <Blink text = 'd'/>
          <Blink text = '!'/>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{width: 275, height: 200, backgroundColor: 'powderblue'}}>
              <Text style= {styles.big}>            Hello</Text>
            </View>

            <View style={{width: 275, height: 200, backgroundColor: 'steelblue'}}>
              <Text style= {styles.big}>World!</Text>
            </View>

          </View>
        
      </View>
      
    );
  }
}



AppRegistry.registerComponent('AwesomeProject', () => BlinkApp);